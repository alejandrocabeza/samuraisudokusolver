#include "Sudoku.cuh"

Sudoku::Sudoku(int x, int y, int ssboard[][21])
{
	terminateFlag = false;

	for (int i = 0; i < 9; ++i)
		for (int j = 0; j < 9; ++j)
			setValue(i, j, ssboard[i + x][j + y]);
}

int Sudoku::getValue(int x, int y)
{
	if (x < 0 || x > 8 || y < 0 || y > 8) return -1;
	return board[x][y];
}

void Sudoku::setValue(int x, int y, int value)
{
	if (x < 0 || x > 8 || y < 0 || y > 8) return;
	board[x][y] = value;
}

int* Sudoku::get1DBoard(int arrayedBoard[])
{
	for (int i = 0; i < 9; ++i)
		for (int j = 0; j < 9; ++j)
			arrayedBoard[i + j * 9] = board[i][j];

	return arrayedBoard;
}

bool Sudoku::initializeBoards(int x_cord, int y_cord, int initBoard[9][9], vector<Array2D> &boards, int amount)
{
	if (amount > 0)
	{
		if (initBoard[x_cord][y_cord] != 0)
		{
			if (verifyValue(x_cord, y_cord, initBoard))
			{
				if (x_cord == 8 && y_cord == 8) return true;

				int next_x = x_cord + 1;
				int next_y = y_cord;

				if (next_x >= 9)
				{
					next_x = 0;
					next_y++;
				}

				return initializeBoards(next_x, next_y, initBoard, boards, amount);
			}
			else return false;
		}

		for (int val = 1; val < 10; val++)
		{
			initBoard[x_cord][y_cord] = val;

			if (verifyValue(x_cord, y_cord, initBoard))
			{
				if (x_cord == 8 && y_cord == 8) return true;

				int next_x = x_cord + 1;
				int next_y = y_cord;

				if (next_x >= 9)
				{
					next_x = 0;
					next_y++;
				}

				Array2D newBoard;
				for (int i = 0; i < 9; ++i) for (int j = 0; j < 9; ++j)newBoard.arr[i][j] = initBoard[i][j];
				boards.push_back(newBoard);

				if (initializeBoards(next_x, next_y, initBoard, boards, amount - 1)) return true;
			}
		}

		initBoard[x_cord][y_cord] = 0;

		return false;
	}

	return true;
}

__global__ bool Sudoku::solveThread(int threadAmount)
{
	//Initialize some boards
	vector <Array2D> boards;

	Array2D newBoard;
	for (int i = 0; i < 9; ++i) for (int j = 0; j < 9; ++j) newBoard.arr[i][j] = board[i][j];
	boards.push_back(newBoard);

	//initBoard Init
	int initBoard[9][9];
	for (int i = 0; i < 9; ++i) for (int j = 0; j < 9; ++j) initBoard[i][j] = board[i][j];

	//Initialize threadAmount of boards
	initializeBoards(0, 0, initBoard, boards, threadAmount - 1);

	//Initialize communication
	bool* finished = new bool[threadAmount];
	for (int i = 0; i < threadAmount; ++i) finished[i] = false;




	//Memory alloc, init and detach
	thread* threads = new thread[threadAmount];
	for (int i = 0; i < threadAmount; ++i) threads[i] = thread(&Sudoku::solve, this, 0, 0, boards[i].arr, finished, i);
	for (int i = 0; i < threadAmount; ++i) threads[i].detach();

	float* boards[9][9];
	cudaMalloc();



	//PENSAR SE IMOS LANZAR UNHA SOLAVEZ OU VARIAS

	solve << <1, 1 >> > (0, 0, boards[i].arr, finished, i);
	cudaDeviceSynchronize();

	//Wait until one of the threads comes up with a solution
	int finishedID = 0;
	while (true)
	{
		if (finishedID >= threadAmount) finishedID = 0;
		if (finished[finishedID]) break;
		++finishedID;
	}
	terminateFlag = true;
	//for (int i = 0; i < threadAmount; ++i) threads[i].~thread();

	//Copy the board to the class one
	for (int i = 0; i < 9; ++i)
		for (int j = 0; j < 9; ++j)
			board[i][j] = boards[finishedID].arr[i][j];

	//Delete
	delete[] finished;
	delete[] threads;

	return true;
}


__device__ bool solve(int x_cord, int y_cord, int myBoard[9][9], bool* finished, int id)
{
	if (myBoard[x_cord][y_cord] != 0)
	{
		if (verifyValue(x_cord, y_cord, myBoard))
		{
			if (x_cord == 8 && y_cord == 8)
			{
				finished[id] = true;
				return true;
			}

			int next_x = x_cord + 1;
			int next_y = y_cord;

			if (next_x >= 9)
			{
				next_x = 0;
				next_y++;
			}

			return solve(next_x, next_y, myBoard, finished, id);
		}
		else return false;
	}

	for (int val = 1; val < 10; val++)
	{
		myBoard[x_cord][y_cord] = val;

		if (verifyValue(x_cord, y_cord, myBoard))
		{
			if (x_cord == 8 && y_cord == 8)
			{
				finished[id] = true;
				return true;
			}

			int next_x = x_cord + 1;
			int next_y = y_cord;

			if (next_x >= 9)
			{
				next_x = 0;
				next_y++;
			}

			if (solve(next_x, next_y, myBoard, finished, id))
			{
				finished[id] = true;
				return true;
			}
		}
	}

	myBoard[x_cord][y_cord] = 0;

	return false;
}

__device__ bool verifyValue(int x_cord, int y_cord, int vBoard[9][9])
{
	int value = vBoard[x_cord][y_cord];

	for (int x_verify = 0; x_verify < 9; x_verify++)
	{
		if (x_verify == x_cord)continue;

		int verifyValue = vBoard[x_verify][y_cord];

		if (verifyValue == value) return false;
	}

	for (int y_verify = 0; y_verify < 9; y_verify++)
	{
		if (y_verify == y_cord) continue;

		int verifyValue = vBoard[x_cord][y_verify];

		if (verifyValue == value) return false;
	}

	int box_x = x_cord / 3;
	int box_y = y_cord / 3;

	for (int y_verify = box_y * 3; y_verify < box_y * 3 + 3; y_verify++)
	{
		for (int x_verify = box_x * 3; x_verify < box_x * 3 + 3; x_verify++)
		{
			if (x_verify == x_cord && y_verify == y_cord) continue;

			int verifyValue = vBoard[x_verify][y_verify];

			if (verifyValue == value) return false;
		}
	}

	return true;
}

void Sudoku::print(int size)
{
	for (int i = 0; i < size; ++i)
	{
		for (int j = 0; j < size; ++j)
		{
			cout << board[i][j];

			if (j < size - 1) cout << " ";
			else cout << endl;
		}
	}
}
