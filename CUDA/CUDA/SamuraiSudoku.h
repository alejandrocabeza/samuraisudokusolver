#include "includes.h"
#include "Sudoku.cuh"

#pragma once

class SamuraiSudoku
{
private:
	int board[21][21];
	void substitute(int x, int y, int arrayedBoard[81]);
	bool isSolved;

public:
	SamuraiSudoku(string path);
	int getValue(int x, int y);
	void setValue(int x, int y, int value);
	void solve();
	void print(int size = 21);
};