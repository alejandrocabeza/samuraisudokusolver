#include <iostream>
#include <fstream>
#include <string>

#include <windows.h>
#include <process.h>    /* _beginthread, _endthread */
#include <stddef.h>
#include <stdlib.h>
#include <conio.h>

#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>

#include <future>

#pragma once

using namespace std;

struct Array2D
{
	int arr[9][9];
};