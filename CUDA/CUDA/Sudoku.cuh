#include "includes.h"

class Sudoku
{
private:
	int board[9][9];
	bool terminateFlag;
	bool verifyValue(int x_cord, int y_cord, int vBoard[9][9]);
	bool solve(int x_cord, int y_cord, int myBoard[9][9], bool* finished, int id);
	bool initializeBoards(int x_cord, int y_cord, int initBoard[9][9], vector<Array2D> &boards, int amount);

public:
	Sudoku(int x, int y, int ssboard[][21]);
	int getValue(int x, int y);
	void setValue(int x, int y, int value);
	int* get1DBoard(int arrayedBoard[]);
	bool solveThread(int threadAmount);
	void print(int size = 9);
};