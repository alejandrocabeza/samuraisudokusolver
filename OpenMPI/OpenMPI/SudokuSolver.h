#ifndef SUDOKUSOLVER_H
#define SUDOKUSOLVER_H

#include "Sudoku.h"

class SudokuSolver
{
private:
	unsigned int threadNumber;
	vector<Sudoku> boards;
	bool* isSolved;
	bool terminateFlag;
	unsigned int solvedSudokus;

	mutex m;
	mutex mi;
	unsigned int communicationID;

	bool verifyValue(int x_cord, int y_cord, int vBoard[9][9]);
	void initializeBoards();
	bool solveSudoku(int x_cord, int y_cord, int myBoard[9][9], int id);
	void solveSudokuEntryPoint(int threadID);

public:
	SudokuSolver(Sudoku s, unsigned int threadNumber);
	~SudokuSolver();
	Sudoku solve();
	void print();
};

#endif