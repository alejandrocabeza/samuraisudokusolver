#ifndef SUDOKUSOLVER_CPP
#define SUDOKUSOLVER_CPP

#include "SudokuSolver.h"

SudokuSolver::SudokuSolver(Sudoku s, unsigned int threadNumber)
{
	this->boards.push_back(s);
	this->threadNumber = threadNumber;
	this->communicationID = 0;
	this->solvedSudokus = 0,

		this->terminateFlag = false;
	this->isSolved = new bool[threadNumber];
	for (unsigned int i = 0; i < threadNumber; ++i) isSolved[i] = FALSE;
};

SudokuSolver::~SudokuSolver()
{
	//Al hacer detach del thread si se llama al destructor petan los threads (que no el programa).
	//delete[] isSolved;
};

bool SudokuSolver::verifyValue(int x_cord, int y_cord, int vBoard[9][9])
{
	int value = vBoard[x_cord][y_cord];

	for (int x_verify = 0; x_verify < 9; x_verify++)
	{
		if (x_verify == x_cord)continue;

		int verifyValue = vBoard[x_verify][y_cord];

		if (verifyValue == value) return false;
	}

	for (int y_verify = 0; y_verify < 9; y_verify++)
	{
		if (y_verify == y_cord) continue;

		int verifyValue = vBoard[x_cord][y_verify];

		if (verifyValue == value) return false;
	}

	int box_x = x_cord / 3;
	int box_y = y_cord / 3;

	for (int y_verify = box_y * 3; y_verify < box_y * 3 + 3; y_verify++)
	{
		for (int x_verify = box_x * 3; x_verify < box_x * 3 + 3; x_verify++)
		{
			if (x_verify == x_cord && y_verify == y_cord) continue;

			int verifyValue = vBoard[x_verify][y_verify];

			if (verifyValue == value) return false;
		}
	}

	return true;
};

void SudokuSolver::initializeBoards()
{
	if (boards.size() == 0)
	{
		cout << "No boards found." << endl;
		return;
	}

	int x = 0;
	int y = 0;
	bool breakLoop = false;

	while (boards.size() < threadNumber)
	{
		Sudoku sudoku = boards[0];
		boards.erase(boards.begin());

		for (int x = 0; x < 9; ++x)
		{
			breakLoop = false;

			for (int y = 0; y < 9; ++y)
			{
				if (sudoku.getValue(x, y) != 0)
				{
					if (x == 8 && y == 8) return;
					continue;
				}

				for (int val = 1; val < 10; ++val)
				{
					Sudoku newSudoku(sudoku);
					newSudoku.setValue(x, y, val);

					if (verifyValue(x, y, newSudoku.board))
					{
						boards.push_back(newSudoku);
					}
				}

				if (x == 8 && y == 8) return;

				breakLoop = true;
				break;
			}

			if (breakLoop) break;
		}
	}

	return;
};

Sudoku SudokuSolver::solve()
{
	initializeBoards();
	thread* solverThreads = new thread[threadNumber];
	for (unsigned int i = 0; i < threadNumber; ++i)
	{
		solverThreads[i] = thread(&SudokuSolver::solveSudokuEntryPoint, this, i);
		solverThreads[i].detach();
	}

	int retID = -1;

	while (true)
	{
		if (solvedSudokus == boards.size())
		{
			cout << "This Samurai Sudoku is unsolvable." << endl;
			Sleep(2000);
			exit(0);
		}

		for (unsigned int i = 0; i < boards.size(); ++i)
		{
			if (isSolved[i])
			{
				retID = i;
				break;
			}
		}

		if (retID >= 0) break;
	}

	terminateFlag = true;
	delete[] solverThreads;
	return boards[retID];
};

void SudokuSolver::solveSudokuEntryPoint(int threadID)
{
	unsigned int myID = 0;
	while (communicationID < boards.size())
	{

		//while (!m.try_lock()) this_thread::yield();

		m.lock();
		myID = communicationID;
		++communicationID;
		m.unlock();

		if (myID >= boards.size()) return;
		solveSudoku(0, 0, boards[myID].board, myID);
	}

	return;
};

bool SudokuSolver::solveSudoku(int x_cord, int y_cord, int myBoard[9][9], int id)
{
	if (terminateFlag) return false;

	if (myBoard[x_cord][y_cord] != 0)
	{
		if (verifyValue(x_cord, y_cord, myBoard))
		{
			if (x_cord == 8 && y_cord == 8)
			{
				isSolved[id] = true;
				++solvedSudokus;
				return true;
			}

			int next_x = x_cord + 1;
			int next_y = y_cord;

			if (next_x >= 9)
			{
				next_x = 0;
				next_y++;
			}

			return solveSudoku(next_x, next_y, myBoard, id);
		}
		else return false;
	}

	for (int val = 1; val < 10; val++)
	{
		myBoard[x_cord][y_cord] = val;

		if (verifyValue(x_cord, y_cord, myBoard))
		{
			if (x_cord == 8 && y_cord == 8)
			{
				isSolved[id] = true;
				++solvedSudokus;
				return true;
			}

			int next_x = x_cord + 1;
			int next_y = y_cord;

			if (next_x >= 9)
			{
				next_x = 0;
				next_y++;
			}

			if (solveSudoku(next_x, next_y, myBoard, id))
			{
				return true;
			}
		}
	}

	myBoard[x_cord][y_cord] = 0;

	isSolved[id] = false;
	return false;
};


void SudokuSolver::print()
{
	for (unsigned int i = 0; i < boards.size(); ++i)
	{
		cout << "State: " << i << endl;

		for (int x = 0; x < 9; ++x)
		{
			for (int y = 0; y < 9; ++y)
			{
				cout << boards[i].getValue(x, y);

				if (y < 8) cout << " ";
				else cout << endl;
			}
		}

		cout << endl << endl;
	}
};

#endif