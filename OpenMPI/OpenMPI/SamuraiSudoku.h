#ifndef SAMURAISUDOKU_H
#define SAMURAISUDOKU_H

#include "includes.h"
#include "SudokuSolver.h"
#include "Sudoku.h"

class SamuraiSudoku
{
private:

	void substitute(int x, int y, int arrayedBoard[81]);
	bool isSolved;

public:
	int board[21][21];
	SamuraiSudoku(string path);
	int getValue(int x, int y);
	void setValue(int x, int y, int value);
	void solve();
	void print(int size = 21);
};

#endif