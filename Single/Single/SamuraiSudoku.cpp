#include "SamuraiSudoku.h"

SamuraiSudoku::SamuraiSudoku(string path)
{
	ifstream file;
	file.open(path);
	int value = 0;

	for (int i = 0; i < 21; ++i)
	{
		for (int j = 0; j < 21; ++j)
		{
			file >> value;
			setValue(i, j, value);
		}
	}

	isSolved = true;
}

int SamuraiSudoku::getValue(int x, int y)
{
	if (x < 0 || x > 20 || y < 0 || y > 20) return -1;
	return board[x][y];
}

void SamuraiSudoku::setValue(int x, int y, int value)
{
	if (x < 0 || x > 20 || y < 0 || y > 20) return;
	board[x][y] = value;
}

void SamuraiSudoku::solve()
{
	Sudoku s1(6, 6, board);
	Sudoku s2(0, 0, board);
	Sudoku s3(12, 0, board);
	Sudoku s4(0, 12, board);
	Sudoku s5(12, 12, board);

	if (!s1.solve()) isSolved = false;
	if (!s2.solve()) isSolved = false;
	if (!s3.solve()) isSolved = false;
	if (!s4.solve()) isSolved = false;
	if (!s5.solve()) isSolved = false;

	int arrayedBoard1[81];
	int arrayedBoard2[81];
	int arrayedBoard3[81];
	int arrayedBoard4[81];
	int arrayedBoard5[81];

	s1.get1DBoard(arrayedBoard1);
	s2.get1DBoard(arrayedBoard2);
	s3.get1DBoard(arrayedBoard3);
	s4.get1DBoard(arrayedBoard4);
	s5.get1DBoard(arrayedBoard5);

	substitute(6, 6, arrayedBoard1);
	substitute(0, 0, arrayedBoard2);
	substitute(12, 0, arrayedBoard3);
	substitute(0, 12, arrayedBoard4);
	substitute(12, 12, arrayedBoard5);
}

void SamuraiSudoku::substitute(int x, int y, int arrayedBoard[81])
{
	for (int i = 0; i < 9; ++i)
		for (int j = 0; j < 9; ++j)
			setValue(i + x, j + y, arrayedBoard[i + j * 9]);
}

void SamuraiSudoku::print(int size)
{
	if (isSolved)
	{
		cout << "The solution for the Samurai Sudoku is:" << endl << endl;

		for (int i = 0; i < size; ++i)
		{
			for (int j = 0; j < size; ++j)
			{
				cout << board[i][j];

				if (j < size - 1) cout << " ";
				else cout << endl;
			}
		}
	}
	else
	{
		cout << "The Samurai Sudoku couldn't be solved." << endl;
	}
}