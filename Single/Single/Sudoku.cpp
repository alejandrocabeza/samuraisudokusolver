#include "Sudoku.h"

Sudoku::Sudoku(int x, int y, int ssboard[][21])
{
	for (int i = 0; i < 9; ++i)
		for (int j = 0; j < 9; ++j)
			setValue(i, j, ssboard[i + x][j + y]);
}

int Sudoku::getValue(int x, int y)
{
	if (x < 0 || x > 8 || y < 0 || y > 8) return -1;
	return board[x][y];
}

void Sudoku::setValue(int x, int y, int value)
{
	if (x < 0 || x > 8 || y < 0 || y > 8) return;
	board[x][y] = value;
}

int* Sudoku::get1DBoard(int arrayedBoard[])
{
	for (int i = 0; i < 9; ++i)
		for (int j = 0; j < 9; ++j)
			arrayedBoard[i + j * 9] = board[i][j];

	return arrayedBoard;
}

bool Sudoku::solve()
{
	return solve(0, 0);
}

bool Sudoku::solve(int x_cord, int y_cord)
{
	if (board[x_cord][y_cord] != 0)
	{
		if (verifyValue(x_cord, y_cord))
		{
			if (x_cord == 8 && y_cord == 8) return true;

			int next_x = x_cord + 1;
			int next_y = y_cord;

			if (next_x >= 9)
			{
				next_x = 0;
				next_y++;
			}

			return solve(next_x, next_y);
		}
		else return false;
	}

	for (int val = 1; val < 10; val++)
	{

		setValue(x_cord, y_cord, val);

		if (verifyValue(x_cord, y_cord))
		{
			if (x_cord == 8 && y_cord == 8) return true;

			int next_x = x_cord + 1;
			int next_y = y_cord;

			if (next_x >= 9)
			{
				next_x = 0;
				next_y++;
			}

			if (solve(next_x, next_y)) return true;
		}
	}

	board[x_cord][y_cord] = 0;

	return false;
}

bool Sudoku::verifyValue(int x_cord, int y_cord)
{
	int value = board[x_cord][y_cord];

	for (int x_verify = 0; x_verify < 9; x_verify++)
	{
		if (x_verify == x_cord)continue;

		int verifyValue = board[x_verify][y_cord];

		if (verifyValue == value) return false;
	}

	for (int y_verify = 0; y_verify < 9; y_verify++)
	{
		if (y_verify == y_cord) continue;

		int verifyValue = board[x_cord][y_verify];

		if (verifyValue == value) return false;
	}

	int box_x = x_cord / 3;
	int box_y = y_cord / 3;

	for (int y_verify = box_y * 3; y_verify < box_y * 3 + 3; y_verify++)
	{
		for (int x_verify = box_x * 3; x_verify < box_x * 3 + 3; x_verify++)
		{
			if (x_verify == x_cord && y_verify == y_cord) continue;

			int verifyValue = board[x_verify][y_verify];

			if (verifyValue == value) return false;
		}
	}

	return true;
}

void Sudoku::print(int size)
{
	for (int i = 0; i < size; ++i)
	{
		for (int j = 0; j < size; ++j)
		{
			cout << board[i][j];

			if (j < size - 1) cout << " ";
			else cout << endl;
		}
	}
}
