#include "includes.h"

class Sudoku
{
private:
	int board[9][9];
	bool verifyValue(int x_cord, int y_cord);
	bool solve(int x_cord, int y_cord);

public:
	Sudoku(int x, int y, int ssboard[][21]);
	int getValue(int x, int y);
	void setValue(int x, int y, int value);
	int* get1DBoard(int arrayedBoard[]);
	bool solve();
	void print(int size = 9);
};