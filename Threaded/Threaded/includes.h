#pragma once

#include <iostream>
#include <fstream>
#include <string>

#include <windows.h>
#include <process.h>    /* _beginthread, _endthread */
#include <stddef.h>
#include <stdlib.h>
#include <conio.h>
#include <mutex>

#include <future>

using namespace std;