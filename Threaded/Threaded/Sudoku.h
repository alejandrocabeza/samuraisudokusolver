#ifndef SUDOKU_H
#define SUDOKU_H

#include "includes.h"

class Sudoku
{
public:
	int board[9][9];

	Sudoku(int x, int y, int ssboard[][21]);
	Sudoku(const Sudoku &s);

	void setValue(int x, int y, int value);
	int getValue(int x, int y) const;

	void getLinearBoard(int linearBoard[81]);

	void print();

	void substituteBoard(int newBoard[][9]);
};

#endif