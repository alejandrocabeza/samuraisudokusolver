#ifndef SAMURAISUDOKU_CPP
#define SAMURAISUDOKU_CPP

#include "SamuraiSudoku.h"

SamuraiSudoku::SamuraiSudoku(string path)
{
	ifstream file;
	file.open(path);
	int value = 0;

	for (int i = 0; i < 21; ++i)
	{
		for (int j = 0; j < 21; ++j)
		{
			file >> value;
			setValue(i, j, value);
		}
	}

	isSolved = true;
};

int SamuraiSudoku::getValue(int x, int y)
{
	if (x < 0 || x > 20 || y < 0 || y > 20) return -1;
	return board[x][y];
};

void SamuraiSudoku::setValue(int x, int y, int value)
{
	if (x < 0 || x > 20 || y < 0 || y > 20) return;
	board[x][y] = value;
};

void SamuraiSudoku::solve()
{
	
	Sudoku s0(6, 6, board);
	Sudoku s1(0, 0, board);
	Sudoku s2(12, 0, board);
	Sudoku s3(0, 12, board);
	Sudoku s4(12, 12, board);

	int threadNum = 8;
	SudokuSolver solver0(s0, threadNum);
	SudokuSolver solver1(s1, threadNum);
	SudokuSolver solver2(s2, threadNum);
	SudokuSolver solver3(s3, threadNum);
	SudokuSolver solver4(s4, threadNum);

	s0 = solver0.solve();
	s1 = solver1.solve();
	s2 = solver2.solve();
	s3 = solver3.solve();
	s4 = solver4.solve();

	int arrayedBoard0[81];
	int arrayedBoard1[81];
	int arrayedBoard2[81];
	int arrayedBoard3[81];
	int arrayedBoard4[81];

	s0.getLinearBoard(arrayedBoard0);
	s1.getLinearBoard(arrayedBoard1);
	s2.getLinearBoard(arrayedBoard2);
	s3.getLinearBoard(arrayedBoard3);
	s4.getLinearBoard(arrayedBoard4);

	substitute(6, 6, arrayedBoard0);
	substitute(0, 0, arrayedBoard1);
	substitute(12, 0, arrayedBoard2);
	substitute(0, 12, arrayedBoard3);
	substitute(12, 12, arrayedBoard4);
};

void SamuraiSudoku::substitute(int x, int y, int arrayedBoard[81])
{
	for (int i = 0; i < 9; ++i)
		for (int j = 0; j < 9; ++j)
			setValue(i + x, j + y, arrayedBoard[i + j * 9]);
};

void SamuraiSudoku::print(int size)
{
	if (isSolved)
	{
		cout << "The solution for the Samurai Sudoku is:" << endl << endl;

		for (int i = 0; i < size; ++i)
		{
			for (int j = 0; j < size; ++j)
			{
				cout << board[i][j];

				if (j < size - 1) cout << " ";
				else cout << endl;
			}
		}
	}
	else
	{
		cout << "The Samurai Sudoku couldn't be solved." << endl;
	}
};

#endif