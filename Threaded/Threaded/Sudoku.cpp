#ifndef SUDOKU_CPP
#define SUDOKU_CPP

#include "Sudoku.h"

Sudoku::Sudoku(int x, int y, int ssboard[][21])
{
	for (int i = 0; i < 9; ++i)
		for (int j = 0; j < 9; ++j)
			setValue(i, j, ssboard[i + x][j + y]);
};

Sudoku::Sudoku(const Sudoku &s)
{
	for (int i = 0; i < 9; ++i)
		for (int j = 0; j < 9; ++j)
			setValue(i, j, s.getValue(i, j));
};

void Sudoku::setValue(int x, int y, int value)
{
	if (x < 0 || x > 8 || y < 0 || y > 8) return;
	board[x][y] = value;
};

int Sudoku::getValue(int x, int y) const
{
	if (x < 0 || x > 8 || y < 0 || y > 8) return -1;
	return board[x][y];
};

void Sudoku::getLinearBoard(int linearBoard[81])
{
	for (int i = 0; i < 9; ++i)
	{
		for (int j = 0; j < 9; ++j)
		{
			linearBoard[i + 9 * j] = getValue(i, j);
		}
	}
};

void Sudoku::print()
{
	for (int i = 0; i < 9; ++i)
	{
		for (int j = 0; j < 9; ++j)
		{
			cout << board[i][j];

			if (j < 8) cout << " ";
			else cout << endl;
		}
	}
};

void Sudoku::substituteBoard(int newBoard[][9])
{
	for (int i = 0; i < 9; ++i)
		for (int j = 0; j < 9; ++j)
			setValue(i, j, newBoard[i][j]);
};

#endif